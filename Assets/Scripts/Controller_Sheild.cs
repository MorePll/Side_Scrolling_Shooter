using UnityEngine;

public class Controller_Shield : MonoBehaviour
{
    public Controller_Player player; // Referencia al objeto Player
    private Rigidbody rb;

    void Start()
    {
        // Inicializar la variable player
        player = FindObjectOfType<Controller_Player>();
        if (player == null)
        {
            Debug.LogError("No se ha encontrado el objeto Controller_Player en la escena.");
        }
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (transform.position != player.transform.position)
        {
            transform.position = player.transform.position;
        }
        Vector3 movement = new Vector3(player.speed * player.inputX, player.speed * player.inputY);
        rb.velocity = movement;
    }
}


