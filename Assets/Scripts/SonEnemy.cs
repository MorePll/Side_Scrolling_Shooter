using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonEnemy : FollowingEnemy
{
    void Awake()
    {
        // Rota el objeto 180 grados en el eje Z antes de que aparezca
        transform.rotation = Quaternion.Euler(0, 0, 180);
    }
    public override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            GeneratePowerUp();
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
            Controller_Hud.points++;
        }
        if (collision.gameObject.CompareTag("Laser"))
        {
            GeneratePowerUp();
            Destroy(this.gameObject);
            Controller_Hud.points++;
        }
    }
}
