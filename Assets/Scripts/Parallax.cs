using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public float parallaxEffect;

    void Update()
    {
        transform.position = new Vector3(transform.position.x - (parallaxEffect / 60), transform.position.y, transform.position.z);
        if (transform.localPosition.x < -3.75f)
        {
            transform.localPosition = new Vector3(13.5f, transform.localPosition.y, transform.localPosition.z);
        }
        if (Controller_Hud.gameOver)
        {
            parallaxEffect = 0;
        }
    }
}
